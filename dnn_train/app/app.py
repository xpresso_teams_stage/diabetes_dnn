"""
This is the implementation of data preparation for sklearn
"""
import os
import sys

from numpy import loadtxt
from keras.models import Sequential
from keras.layers import Dense
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger



__author__ = "### Anushree ###"

logging = XprLogger("dnn_train")

DEFAULT_BATCH_SIZE = 10
DEFAULT_TRAIN_EPOCHS = 100


class DnnTrain(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self,file_path):
        super().__init__(name="DnnTrain")
        self.dataset = loadtxt(file_path, delimiter=',')
        self.model = Sequential()
        """ Initialize all the required constansts and data her """

    def start(self, run_name, batch_size = DEFAULT_BATCH_SIZE, train_eopchs = DEFAULT_TRAIN_EPOCHS):

        super().start(xpresso_run_name=run_name)
        self.batch_size = batch_size
        self.train_eopchs = train_eopchs
        logging.info("preparing dataset")
        # split into input (X) and output (y) variables
        X = self.dataset[:, 0:8]
        y = self.dataset[:, 8]

        logging.info("Training model")

        # define the keras model
        self.model = Sequential()
        self.model.add(Dense(12, input_dim=8, activation='relu'))
        self.model.add(Dense(8, activation='relu'))
        self.model.add(Dense(1))

        # compile the keras model
        self.model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

        self.model.fit(X, y,
                       batch_size=self.batch_size,
                       epochs=self.train_eopchs)

        # evaluate the keras model
        _, accuracy = self.model.evaluate(X, y)
        print('Accuracy: %.2f' % (accuracy * 100))
        self.send_metrics("Model Accuracy: ", accuracy * 100)
        logging.info(accuracy*100)

        self.completed()

    def send_metrics(self,status, accuracy):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": "data_prediction"},
            "metric": {"accuracy": accuracy}
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        logging.info("Saving model")

        if not os.path.exists(self.OUTPUT_DIR):
            os.makedirs(self.OUTPUT_DIR)
        self.model.save(os.path.join(self.OUTPUT_DIR, 'saved_model_train.h5'))
        logging.info("Saved model... load model")
        super().completed(push_exp=True)



if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    dnn_train = DnnTrain(file_path="/data/diabetes_data/diabetes.csv")
    if len(sys.argv) >= 2:
        dnn_train.start(run_name=sys.argv[1], batch_size=int(sys.argv[2]), train_eopchs=int(sys.argv[3]))
    else:
        dnn_train.start(run_name="")
