"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""
import os
import numpy as np
import sys
from keras.models import load_model
from numpy import loadtxt
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Anushree ###"

logging = XprLogger("dnn_infer")


class DnnInfer(AbstractInferenceService):
    """ This is the main class for inference service. It implements predict and
    supporting functionality. REST API is automatically gengerated by the
    Abstract inference service """

    def __init__(self,file_path):
        super().__init__()
        self.dataset = loadtxt(file_path, delimiter=',')
        self.x = self.dataset[:, 0:8]
        self.y = self.dataset[:, 8]



    def load_model(self, model_path):
        """ Loads the model into memory """
        self.model =load_model(os.path.join(model_path, "saved_model1.h5"))

    def transform_input(self):
        """ Receives the json input data from the Rest API. This is converted
        into a dataframe object with the provided input """

        return 1


    def transform_output(self, output_response):
        """ This prepares the response. Output from predicts is converted into
        a response json serializable object. No need to change anything
        here """
        return output_response

    def predict(self, x,y):
        """ Main predict function for predicting the sales """
        logging.info("Loading dataset to split")
        X_train, X_test, y_train, y_test = train_test_split(self.x, self.y, test_size=0.3)
        logging.info(X_test, y_test)
        y_pred = self.model.predict(X_test)
        y_pred1=np.rint(y_pred)
        score = accuracy_score(y_true=y_test, y_pred=y_pred1)
        print(score)
        return score,y_pred1

    def report_inference_status(self, service_info, status):
        " Not in user right now "
        pass


if __name__ == "__main__":
    """ Main file to start the execution 
    It takes run_id parameters from command line"""

    pred = DnnInfer(file_path="/data/diabetes_data/diabetes.csv")
    pred.load()
    pred.run_api(port=5000)